FROM python:3
ENV PYTHONUNBUFFERED 1
RUN mkdir /autenticacion
WORKDIR /autenticacion
ADD . /autenticacion/
RUN pip install -r requirements.txt
EXPOSE 9000
CMD python manage.py makemigrations && python manage.py migrate && python manage.py runserver 0.0.0.0:$PORT