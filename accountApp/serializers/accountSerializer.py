from rest_framework import serializers
from accountApp.models.account import Account

class AccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = ['id', 'name', 'lastname', 'position', 'email']